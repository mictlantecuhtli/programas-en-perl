#!usr/bin/perl
#Fecha: 11_04_2021
#Autor: Fatima Azucena MC
#Correo: fatimaazucenamartinez274@gmail.com

use strict;
my $a1;
my $b1;
my $a2;
my $b2;
my $x;
my $y;
my $dx;
my $dy;
my $p;

	print("Digite el valor de la coordenada x1: ");
	$a1 = <STDIN>;
	print("Digite el valor de la coordenada y1: ");
	$b1 = <STDIN>;
	print("Digite el valor de la coordenada x2: ");
	$a2 = <STDIN>;
	print("Digite el valor de la coordenada y2: ");
	$b2 = <STDIN>;
	#chomp($a1);
	#chomp($a2);
	#chomp($b1);
	#chomp($b2);
	algoritmoBresenham ();


sub algoritmoBresenham {
	$x = $a1;
	$y = $b1;
	$dx = $a2 - $a1;
	$dy = $b2 - $b1;
	$p = (2*$dy) - $dx;

	while ( $x <= $a2 ){
		print("x:$x y:$y\n");
		$x = $x + 1;
		if ( $p < 0){
			$p = $p + 2 * $dy;
		}
		else {
			$p = $p + ( 2 * $dy ) - ( 2 * $dx ); 
			$y = $y + 1; 
		}
	}
}
