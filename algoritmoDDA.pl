#!usr/bin/perl
#Fecha: 11_04_2021
#Autor: Fatima Azucena MC
#Correo: fatimaazucenamartinez274@gmail.com

use strict;
my $x1;
my $x2;
my $y1;
my $y2;
my $i;
my $dx;
my $dy;
my $steps;
my $xinc;
my $yinc;

 	print "Digite el valor de la coordenada x1: ";
        $x1 = <STDIN>;
        print "Digite el valor de la coordenada y1: ";
        $y1 = <STDIN>;
        print "Digite el valor de la coordenada x2: ";
        $x2 = <STDIN>;
        print "Digite el valor de la coordenada y2: ";
        $y2 = <STDIN>;
	algoritmoDDA();


sub algoritmoDDA {
	#abs($a1);
	#abs($a2);
	#abs($b1);
	#abs($b2);
	$dx = abs($x2 - $x1);
	$dy = ($y2 - $y1);

	if ($dx > $dy){
		$steps = $dx;
	}
	else {
		$steps = $dy;
	}

	$xinc = $dx / $steps;
	$yinc = $dy / $steps;
	
	for ($i = 0; $i <= $steps; $i++){
		print("x:$x1 y:$y1\n");
		$x1 = $x1 + $xinc;
		$y1 = $y1 + $yinc;
	}
}
