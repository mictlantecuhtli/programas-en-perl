#!usr/bin/perl
#Fecha: 11_04_2021
#Autor: Fatima Azucena MC
#Correo: fatimaazucenamartinez274@gmail.com

use strict;
my $opcion;
my $entrada;
my $salida;
my $valor_entrada;
my $resultado;
my @nombres = ("Longitud","Area","Volumen","Masa","Velocidad","Densidad","Fuerza","Energia","Potencia","Temperatura");
my @temperatura = ("C°","F°","K°");
my @array = (
	["mm","cm","in","ft","y","m","km","mi"],
	["m^2","ft^2","mi^2","acre","No válido","No válido","No válido","No válido"],
	["litro","m^3","ft^3","galon americano","galon britanico","No válido","No válido","No válido"],
	["kg","lb","gm","slug","No válido","No válido","No válido","No válido"],
	["km/hr","mi/hr","p/s","m/s","No válido","No válido","No válido","No válido"],
	["gm/cm^3","lb/ft^3","kg/m^3","slug/ft^3","No válido","No válido","No válido","No válido"],
	["N","lbf","kgf","dinas","No válido","No válido","No válido","No válido"],
	["J","Nm","cal","Btu","lbf/ft","w/hr","kw/hr","eV"],
	["w","j/s","erg/s","cal/h","hp","kw","btu/s","No válido"],
	["N/m^2","lbf/in^2","atm","d/cm^2","lbf/ft^2","pulgada de agua","No válido","No válido"]
);
my @valores = (
	[1000,100,41.66,3.28083,1.09,1,0.001,0.0006215],
	[0.0929,1,0.000087,0.000022957,0,0,0,0],
	[1,0.001,0.03531147,0.264172,0.219909,0,0,0],
	[0.453592,1,453.59,0.031,0,0,0,0],
	[1,0.6213,0.91134,0.2777,0,0,0,0],
	[1,62.428,1000,1.94032,0,0,0,0],
	[1,0.2248,0.101972,100000,0,0,0,0],
	[4184,1,1000,3.96567,3085.96,1.16222,0.00116222,2670.68],
	[745.7,745.7,7456998715.823,8437231,1,0.7457,0.706787,0,0],
	[1,0.000145,0.0000098,10,0.02088,0.000750,0.00401,0]
);
	for (my $i = 0; $i < 10; $i++){
		print("$i.-$nombres[$i]\n");
	}
	print ("Digite su opcion: ");
	$opcion = <STDIN>;
	chomp($opcion);
	if ($opcion == 9){
		for (my $i = 0; $i < 3; $i++){
			print("$i.-$temperatura[$i]\n");
		}

		print ("Digite la unidad de medida de entrada: ");
		$entrada = <STDIN>;
		print ("Digite la unidad de medida de salida: ");
		$salida = <STDIN>;
		print ("Digite el valor numerico de entrada: ");
		$valor_entrada = <STDIN>;

		if (($entrada == 0)&&($salida == 1)){
			$resultado = ($valor_entrada*1.8)+32;
			print("La conversion es: $resultado\n");
		}
		elsif (($entrada == 0)&&($salida == 2)){
			$resultado = $valor_entrada + 273.15;
			print("La conversion es: $resultado\n");
		}
		elsif (($entrada == 1)&&($salida == 0)){
			$resultado = ($valor_entrada-32)*0.5555555556;
			print("La conversion es: $resultado\n");
		}
		elsif (($entrada == 1)&&($salida == 2)){
			$resultado = ($valor_entrada-32)*0.5555555556+273.15;
			print("La conversion es: $resultado\n");
		}
		elsif (($entrada == 2)&&($salida == 0)){
			$resultado = $valor_entrada-273.15;
			print("La conversion es: $resultado\n");
		}
		elsif (($entrada == 2)&&($salida == 1)){
			$resultado = ($valor_entrada-273.15)*1.8+32;
			print("La conversion es: $resultado\n");
		}
	}
	else { 
		desprender_menu();
		print ("Digite la unidad de medidad de entrada: ");
		$entrada = <STDIN>;
		print ("Digite la unidad de medida de salida: ");
		$salida = <STDIN>;
		print ("Digite el valor numerico de entrada: ");
		$valor_entrada = <STDIN>;
		$resultado = ($valores[$opcion][$salida]/$valores[$opcion][$entrada])*$valor_entrada;
		print ("La conversion es: $resultado\n");
	}
	
sub desprender_menu{
	for (my $i = 0; $i < 8; $i++){
		print("$i.-$array[$opcion][$i]\n");
	}
}	
