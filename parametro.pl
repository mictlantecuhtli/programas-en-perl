#!/usr/bin/perl 
  
# Defining Subroutine
sub Display_List 
{ 
      
    # array variable to store 
    # the passed arguments 
    my @List1 = @_; 
          
    # Printing the passed list elements 
    print "Given list is @List1\n"; 
} 
  
# Driver Code
  
# passing list 
@list = (1, 2, 3, 4); 
  
# Calling Subroutine with 
# list parameter 
Display_List(@list); 
