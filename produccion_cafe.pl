#!usr/bin/perl
#Fecha: 11_04_2021
#Autor: Fatima Azucena MC
#Correo: fatimaazucenamartinez274@gmail.com

use strict;
my $posicion;
my $total_produccion;
my @nombres = ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
my @suma_produccion = [100];

print("Digite la produccion de cáfe en KG por meses: \n");
for (my $i = 0; $i < 12; $i++){
	print("--> $nombres[$i]\n");
	$suma_produccion[$i] = <STDIN>;
	chomp($suma_produccion[$i]);
	$total_produccion = $total_produccion + $suma_produccion[$i];
}

$total_produccion = $total_produccion / 12;
print ("La produccion anual es de: $total_produccion kg");

print("\nMayor produccion que el promedio: \n");
for (my $i = 0; $i < 12; $i++){
	if ($suma_produccion[$i] > $total_produccion){
		print("--> $suma_produccion[$i]\n");
	}
}

print("Menor produccion que el promedio: \n");
for (my $i = 0; $i < 12; $i++){
        if ($suma_produccion[$i] < $total_produccion){
                print("--> $suma_produccion[$i]\n");
        }
}

for (my $i = 0; $i < 12; $i++){
	for (my $y = 0; $y < 11; $y++){
		if ($suma_produccion[$y] > $suma_produccion[$y+1]){
			$posicion = $suma_produccion[$y+1];
			$suma_produccion[$y+1] = $suma_produccion[$y];
			$suma_produccion[$y] = $posicion
		}
	}
}
print("La mayor produccion de cáfe es de: $suma_produccion[11]");
print("\nLa menor produccion de cáfe es de: $suma_produccion[0]");

