  #!/usr/bin/perl
  
  $uno=23.34;
  $dos=26.66;

  #Llamamos a la función suma
  $sum=suma($uno,$dos);

  print "La suma de $uno + $dos es $sum\n";

  #FUnción suma
  sub suma
  {
    #Cogemos los parámetros
    my ($primero,$segundo)=@_; #También se podría hacer con my $primero=shift(@_); my $segundo shift(@_)];

    #Devolvemos el resultado
    return ($primero+$segundo);
  }

